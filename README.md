era AWT for Korean 한국어화 설명서
==================================

## 개요
이 프로젝트는 'era AWT'의 2.15.2버전에서 분기된 한국어용 포크입니다.

era AWT 3.0.0부터는 UI 등이 크게 바뀌어 그에 맞춰 새로이 작업해야하는 상황이며, 게임 자체는 2.15.2버전에서 안정화된 상태입니다. 따라서 era AWT의 새 버전이 나와도 해당 버전으로의 마이그레이션을 하지 않을 예정이며, 이 프로젝트 내에서 발견되는 버그 및 개선사항은 자체적으로 해결하는 것으로 하겠습니다.

만약 이후 버전의 한국어화가 필요하면 별도로 프로젝트를 만들어 작업하셔도 좋습니다.

한국어화가 완료될 때까지 일부 버그 수정 및 부분적인 시스템 개선 외에는 추가사항은 없습니다.

## 플레이에 관하여
'Web IDE' 버튼 옆의 📥와 비슷한 아이콘을 클릭하여 다운로드 받습니다.

기본 폰트로 '[D2Coding](https://github.com/naver/d2codingfont)'을 사용합니다. 게임 실행전에 설치해주셔야 인게임 레이아웃이 깨지지 않습니다.

Emuera의 모바일 복제판인 [uEmuera](https://github.com/xerysherry/uEmuera)에서의 정상적인 플레이 가능 여부는 확인하지 못했습니다.

아직 번역이 완전하지 않은 관계로 동봉된 [ezEmuera](https://github.com/Riey/ezEmuera/tree/master/Emuera)로 플레이하시면 될 것 같습니다. 한국어와 일본어가 섞여 기계 번역 품질이 안좋아질 수도 있습니다.

## 한국어화 기여 방법
한국어화 작업은 [Gitlab]을 통해 이루어집니다. [Gitlab]은 [Github]와 유사한 Git 호스팅사이트며 사용방법도 비슷합니다.

이 프로젝트에 기여하고 싶으시다면 프로젝트를 Fork하여 '[develop]' 브랜치에서 작업하여 Commit한 후 [Merge Request]합니다. ('Merge Request'는 [Github]의 'Pull Request'와 동일한 기능입니다.)

[develop] 브랜치가 아닌 [master] 브랜치에 [Merge Request]하면 거부되거나 잔소리를 들을 수 있습니다. 간단하게 [Git 사용방법](https://www.google.com/search?q=git+%EC%82%AC%EC%9A%A9%EB%B2%95)을 숙지하시는 것이 좋습니다.

번역 중 기술적인 궁금증은 [이슈트래커](https://gitlab.com/miqotechan/era-awt-for-korean/issues)에 질문 남겨주시면 답변드리겠습니다.

[Merge Request]:https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[develop]:https://gitlab.com/miqotechan/era-awt-for-korean/tree/develop
[master]:https://gitlab.com/miqotechan/era-awt-for-korean/tree/master
[Gitlab]:https://gitlab.com/
[Github]:https://github.com/

## 한글 유틸
era AWT for Korean은 한국어화 작업을 위해 자체적인 한글 유틸 스크립트를 만들어 사용중입니다. 한글 유틸에 관한 정보는 [README_한글유틸]문서를 참고해주세요.

[README_한글유틸]:README_한글유틸.md

## 사용 조건 및 면책 사항
©2019 미코테짱

'era AWT for Korean'은 원작 'era AWT'와 마찬가지로 비영리 목적으로의 복제, 수정, 배포가 가능하며 어떠한 문제가 발생하더라도 원작자 'No.76'와 수정자 '미코테짱'은 책임지지 않습니다.

이 사항은 'No.76'과 '미코테짱'이 작성하지 않은 'Emuera'소프트웨어를 포함한 저작물에는 적용되지 않습니다.

'era AWT for Korean' 기여자는 각각 자신의 저작물에 대하여 권리를 가집니다. 그러나 해당 저작물이 이 프로젝트에 기여될 경우 해당 저작물에 대한 사용권 및 수정권을 이 프로젝트에 양도하는 것으로 간주합니다. 양도된 사용권은 철회 가능합니다.

--------------------------------

## ERB 폴더 구조
아래에서 설명된 폴더 외에도 하위 폴더가 많습니다. 폴더 명칭 및 파일 이름 명칭은 수가 많아 직접적으로 번역해놓지 않았습니다.
* 📁01_DIM_変数 (변수)
* [📁02_FUNC_関数](#02_func_関数) (함수)
* [📁03_TITLE_起動](#03_title_起動) (기동)
* [📁04_FIRST_開始](#04_first_開始) (시작)
* [📁05_SHOP_主要](#05_shop_主要) (주요)
* [📁06_TEXT_口上](#06_text_口上) (이야기)

## ERB 폴더 구조 설명
### 📁02_FUNC_関数
다른 스크립트의 기능을 보조하는 함수들입니다. 텍스트를 반환하는 함수도 꽤 많은 편이며 다른 문장과 조합되어 쓰이는 경우가 많습니다.

일부 화면은 설정 페이지로써 화면 묘사에 이용되는 함수도 몇몇 있습니다.

### 📁03_TITLE_起動
타이틀 화면을 묘사하는 스크립트들입니다. '03_TITLE_01_メイン.ERB'외의 다른 스크립트들은 타이틀 하단의 부가 설명 화면을 표시하는 데에 쓰입니다.

### 📁04_FIRST_開始
'New Game'시 초기 변수를 설정하는 스크립트들입니다. 플레이어에게 초기 설정 화면을 표시하고 첫 이야기를 시작합니다.

### 📁05_SHOP_主要
게임 화면의 전반적인 묘사를 맡고 있습니다. 메인 화면, 전투, 채집, 정원 가꾸기 등의 시스템적인 요소로 보이는 모든 것을 처리하고 출력합니다.

### 📁06_TEXT_口上
이야기를 묘사하는데 초점이 맞춰진 스크립트들입니다. 각 상황에 대해서 지문이 각 캐릭터들에게 맞추어지거나 랜덤한 지문을 묘사하기도합니다.
가장 번역 필요량이 많습니다.